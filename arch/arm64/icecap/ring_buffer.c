#include <linux/irq.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>

#include "sel4.h"
#include "ring_buffer.h"
#include "vmm.h"

// HACK
#define assert(cond) BUG_ON(!(cond))

void icecap_ring_buffer_kick(icecap_ring_buffer_kick_t *kick)
{
	int this_cpu;
	seL4_MessageInfo_t req;
	seL4_MessageInfo_t resp = ~0;
	uint64_t mr0;
	switch (kick->type) {
		case ICECAP_RING_BUFFER_KICK_TYPE_UNMANAGED:
			seL4_Signal(kick->value.unmanaged.notification);
			break;
		case ICECAP_RING_BUFFER_KICK_TYPE_MANAGED:
			mr0 = kick->value.managed.message;
			req = seL4_MessageInfo_new(0, 0, 0, 1);
			this_cpu = get_cpu();
			resp = seL4_CallWithMRs(kick->value.managed.endpoints[this_cpu], req, &mr0, 0, 0, 0);
			put_cpu();
			BUG_ON(seL4_MessageInfo_get_length(resp) != 0);
			break;
		default:
			BUG();
	}
}

size_t icecap_ring_buffer_poll_read(icecap_ring_buffer_t *b)
{
	size_t offset_r;
	size_t offset_w;
	offset_r = b->private_offset_r;
	offset_w = READ_ONCE(b->read.ctrl->offset_w);
	assert(offset_r <= offset_w);
	assert(offset_w - offset_r <= b->read.size);
	return offset_w - offset_r;
}

size_t icecap_ring_buffer_poll_write(icecap_ring_buffer_t *b)
{
	size_t offset_r;
	size_t offset_w;
	offset_r = READ_ONCE(b->read.ctrl->offset_r);
	offset_w = b->private_offset_w;
	assert(offset_r <= offset_w);
	assert(offset_w - offset_r <= b->write.size);
	return b->write.size - (offset_w - offset_r);
}

void icecap_ring_buffer_read(icecap_ring_buffer_t *b, size_t n, char *buf)
{
	icecap_ring_buffer_peek(b, n, buf);
	icecap_ring_buffer_skip(b, n);
}

void icecap_ring_buffer_read_to_user(icecap_ring_buffer_t *b, size_t n, char __user *buf)
{
	icecap_ring_buffer_peek_to_user(b, n, buf);
	icecap_ring_buffer_skip(b, n);
}

void icecap_ring_buffer_skip(icecap_ring_buffer_t *b, size_t n)
{
	assert(n <= icecap_ring_buffer_poll_read(b));
	b->private_offset_r += n;
}

void icecap_ring_buffer_peek(icecap_ring_buffer_t *b, size_t n, char *buf)
{
	size_t offset = b->private_offset_r % b->read.size;
	size_t n1 = b->read.size - offset;
	assert(n <= icecap_ring_buffer_poll_read(b));
	if (n <= n1)
		memcpy(buf, b->read.buf + offset, n);
	else {
		memcpy(buf, b->read.buf + offset, n1);
		memcpy(buf + n1, b->read.buf, n - n1);
	}
}

void icecap_ring_buffer_peek_to_user(icecap_ring_buffer_t *b, size_t n, char __user *buf)
{
	size_t offset = b->private_offset_r % b->read.size;
	size_t n1 = b->read.size - offset;
	assert(n <= icecap_ring_buffer_poll_read(b));
	if (n <= n1)
		BUG_ON(copy_to_user(buf, b->read.buf + offset, n));
	else {
		BUG_ON(copy_to_user(buf, b->read.buf + offset, n1));
		BUG_ON(copy_to_user(buf + n1, b->read.buf, n - n1));
	}
}

void icecap_ring_buffer_write(icecap_ring_buffer_t *b, size_t n, const char *buf)
{
	size_t offset = b->private_offset_w % b->write.size;
	size_t n1 = b->write.size - offset;
	assert(n <= icecap_ring_buffer_poll_write(b));
	if (n <= n1) {
		memcpy(b->write.buf + offset, buf, n);
	} else {
		memcpy(b->write.buf + offset, buf, n1);
		memcpy(b->write.buf, buf + n1, n - n1);
	}
	b->private_offset_w += n;
}

void icecap_ring_buffer_write_from_user(icecap_ring_buffer_t *b, size_t n, const char __user *buf)
{
	size_t offset = b->private_offset_w % b->write.size;
	size_t n1 = b->write.size - offset;
	assert(n <= icecap_ring_buffer_poll_write(b));
	if (n <= n1) {
		BUG_ON(copy_from_user(b->write.buf + offset, buf, n));
	} else {
		BUG_ON(copy_from_user(b->write.buf + offset, buf, n1));
		BUG_ON(copy_from_user(b->write.buf, buf + n1, n - n1));
	}
	b->private_offset_w += n;
}

int icecap_ring_buffer_poll_read_packet(icecap_ring_buffer_t *b, size_t *n)
{
	icecap_ring_buffer_packet_header_t header;
	if (icecap_ring_buffer_poll_read(b) < sizeof(header)) {
		return ICECAP_RING_BUFFER_NO_PACKET;
	}
	icecap_ring_buffer_peek(b, sizeof(header), (char *)&header);
	if (icecap_ring_buffer_poll_read(b) < sizeof(header) + header) {
		return ICECAP_RING_BUFFER_NO_PACKET;
	};
	*n = header;
	return ICECAP_RING_BUFFER_PACKET;
}

void icecap_ring_buffer_read_packet(icecap_ring_buffer_t *b, size_t n, char *buf)
{
	BUG_ON({
		size_t obs_n;
		((icecap_ring_buffer_poll_read_packet(b, &obs_n) == ICECAP_RING_BUFFER_NO_PACKET) || obs_n != n);
	});
	icecap_ring_buffer_skip(b, sizeof(icecap_ring_buffer_packet_header_t));
	icecap_ring_buffer_read(b, n, buf);
}

int icecap_ring_buffer_poll_write_packet(icecap_ring_buffer_t *b, size_t n)
{
	if (icecap_ring_buffer_poll_write(b) < sizeof(icecap_ring_buffer_packet_header_t) + n) {
		return ICECAP_RING_BUFFER_NO_PACKET;
	} else {
		return ICECAP_RING_BUFFER_PACKET;
	}
}

void icecap_ring_buffer_write_packet(icecap_ring_buffer_t *b, size_t n, const char *buf)
{
	icecap_ring_buffer_packet_header_t header;
	BUG_ON(icecap_ring_buffer_poll_write_packet(b, n) == ICECAP_RING_BUFFER_NO_PACKET);
	header = (icecap_ring_buffer_packet_header_t)n; // HACK
	icecap_ring_buffer_write(b, sizeof(header), (char *)&header);
	icecap_ring_buffer_write(b, n, buf);
}

void icecap_ring_buffer_notify_read(icecap_ring_buffer_t *b)
{
	virt_mb();
	WRITE_ONCE(b->write.ctrl->offset_r, b->private_offset_r);
	virt_mb();
	if (test_and_clear_bit(ICECAP_RING_BUFFER_S_NOTIFY_WRITE, (volatile unsigned long *)&b->read.ctrl->status)) {
		virt_mb();
		icecap_ring_buffer_kick(&b->kick);
	}
}

void icecap_ring_buffer_notify_write(icecap_ring_buffer_t *b)
{
	virt_mb();
	WRITE_ONCE(b->write.ctrl->offset_w, b->private_offset_w);
	virt_mb();
	if (test_and_clear_bit(ICECAP_RING_BUFFER_S_NOTIFY_READ, (volatile unsigned long *)&b->read.ctrl->status)) {
		virt_mb();
		icecap_ring_buffer_kick(&b->kick);
	}
}

void icecap_ring_buffer_enable_notify_read(icecap_ring_buffer_t *b)
{
	set_bit(ICECAP_RING_BUFFER_S_NOTIFY_READ, (volatile unsigned long *)&b->write.ctrl->status);
}

void icecap_ring_buffer_enable_notify_write(icecap_ring_buffer_t *b)
{
	set_bit(ICECAP_RING_BUFFER_S_NOTIFY_WRITE, (volatile unsigned long *)&b->write.ctrl->status);
}

void icecap_ring_buffer_disable_notify_read(icecap_ring_buffer_t *b)
{
	clear_bit(ICECAP_RING_BUFFER_S_NOTIFY_READ, (volatile unsigned long *)&b->write.ctrl->status);
}

void icecap_ring_buffer_disable_notify_write(icecap_ring_buffer_t *b)
{
	clear_bit(ICECAP_RING_BUFFER_S_NOTIFY_WRITE, (volatile unsigned long *)&b->write.ctrl->status);
}

void icecap_ring_buffer_of(struct platform_device *dev, icecap_ring_buffer_t *rb)
{
	int err;
	struct resource *r;
	const char *kick_type = 0;

	// TODO assert size = 2**e

	r = platform_get_resource(dev, IORESOURCE_MEM, 0);
	BUG_ON(!r);
	BUG_ON(resource_size(r) != ICECAP_RING_BUFFER_CTRL_SIZE);
	rb->read.ctrl = (icecap_ring_buffer_ctrl_t *)memremap(r->start, resource_size(r), MEMREMAP_WB);
	BUG_ON(!rb->read.ctrl);

	r = platform_get_resource(dev, IORESOURCE_MEM, 1);
	BUG_ON(!r);
	BUG_ON(resource_size(r) != ICECAP_RING_BUFFER_CTRL_SIZE);
	rb->write.ctrl = (icecap_ring_buffer_ctrl_t *)memremap(r->start, resource_size(r), MEMREMAP_WB);
	BUG_ON(!rb->write.ctrl);

	r = platform_get_resource(dev, IORESOURCE_MEM, 2);
	BUG_ON(!r);
	rb->read.size = resource_size(r);
	rb->read.buf = (char *)memremap(r->start, resource_size(r), MEMREMAP_WB);
	BUG_ON(!rb->read.buf);

	r = platform_get_resource(dev, IORESOURCE_MEM, 3);
	BUG_ON(!r);
	rb->write.size = resource_size(r);
	rb->write.buf = (char *)memremap(r->start, resource_size(r), MEMREMAP_WB);
	BUG_ON(!rb->write.buf);

	err = of_property_read_string(dev->dev.of_node, "kick-type", &kick_type);
	BUG_ON(err);
	BUG_ON(!kick_type);
	if (strcmp(kick_type, "unmanaged") == 0) {
		rb->kick.type = ICECAP_RING_BUFFER_KICK_TYPE_UNMANAGED;
		err = of_property_read_u64(dev->dev.of_node, "kick-notification", &rb->kick.value.unmanaged.notification);
		BUG_ON(err);
		printk("icecap-ring-buffer: %s: kick is unmanaged with notification=%lld", dev->name, rb->kick.value.unmanaged.notification);
	} else if (strcmp(kick_type, "managed") == 0) {
		rb->kick.type = ICECAP_RING_BUFFER_KICK_TYPE_MANAGED;
		err = of_property_read_u64(dev->dev.of_node, "kick-message", &rb->kick.value.managed.message);
		BUG_ON(err);
		err = of_property_read_u64_array(dev->dev.of_node, "kick-endpoints", &rb->kick.value.managed.endpoints[0], num_online_cpus());
		BUG_ON(err);
		printk("icecap-ring-buffer: %s: kick is managed with message=%lld", dev->name, rb->kick.value.managed.message);
	} else {
		BUG();
	}

	// HACK
	rb->private_offset_r = rb->write.ctrl->offset_r;
	rb->private_offset_w = rb->write.ctrl->offset_w;
}
